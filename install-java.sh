#!/bin/bash
# -*- coding: utf-8 -*-

##
# =============================================================================
# Installaiton helper for Oracle JDK or JRE on Ubuntu 
#
# Author : Pascal Rapaz
# Version: 1.2.0
# Date   : 17.10.2013 Creation
#          16.09.2014 Version independant
#          02.10.2014 Create .jinfo file, complete update-alternatives
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2013-2014, Pascal Rapaz (RapazP - pascal.rapaz@rapazp.ch)
# =============================================================================
##

DEST=/usr/lib/jvm

# list used to generate .jinfo file
# because there is only few informations about this file, I can't ensure that
# all exucutable files are in the good category
HL='keytool orbd pack200 rmid rmiregistry servertool tnameserv unpack200'
JRE='ControlPanel java javaws jcontrol policytool'
JDK='appletviewer extcheck idlj jar jarsigner javac javadoc javafxpackager javah javap jcmd jconsole jdb jhat jinfo jmap jmc jps jrunscript jsadebugd jstack jstat jstatd jvisualvm native2ascii rmic schemagen serialver wsgen wsimport xjc '

PLUGIN='libnpjp2.so'
BROWSER='firefox-addons iceape iceweasel midbrowser mozilla xulrunner'

function split_tarfile {
  if [[ $1 != *.tar.gz ]]; then
    usage
    exit 1
  fi

  # split the command arguement ($1 => tar.gz file name)
  arr=(${1//-/ })

  # get the java type jre or jdk
  TYPE=(${arr//\// })
  TYPE=${TYPE[-1]}

  # retrieve major and minor version
  ver=(${arr[1]//u/ })
  MAJOR=1.${ver[0]}.0
  MINOR=${ver[1]}
  PRIORITY=${ver[0]}${ver[1]}

  # retrieve OS type
  machine_type=$(uname -m)
  if [ "${machine_type}" == 'x86_64' ]; then
    BITS='amd64'
  else
    BITS='i386'
  fi
}

function install {
  # remove old version and install the new one
  mkdir -p ${DEST}
  rm "${DEST}/${TYPE}${MAJOR}*" -rf
  tar xfz "$1" -C ${DEST}

  # create symbolic links
  ln -sfn ${DEST}/${TYPE}${MAJOR}_${MINOR} ${DEST}/${TYPE}${MAJOR}
}

function create_jinfo {
  file_name=.${TYPE}${MAJOR}_${MINOR}.jinfo
  path=${DEST}/${file_name}

  cat <<EOF > ${path}
name=${TYPE}${MAJOR}_${MINOR}
alias=${TYPE}${MAJOR}
priority=${PRIORITY}
section=non-free

EOF

  for i in ${HL}; do
    echo  'hl' "${i}" ${DEST}/${TYPE}${MAJOR}/bin/"${i}" >> ${path}
  done

  for i in ${JRE}; do
    echo 'jre' "${i}" ${DEST}/${TYPE}${MAJOR}/bin/"${i}" >> ${path}
  done

  for i in ${JDK}; do
    echo 'jdk' "${i}" ${DEST}/${TYPE}${MAJOR}/bin/"${i}" >> ${path}
  done

  for i in ${BROWSER}; do
    echo 'plugin' "${i}"-javaplugin.so ${DEST}/${TYPE}${MAJOR}/jre/lib/${BITS}/${PLUGIN} >> ${path}
  done
}

function update_alternatives {
  for i in ${HL}; do
    update-alternatives --install /usr/bin/"${i}" "${i}" ${DEST}/${TYPE}${MAJOR}/bin/"${i}" ${PRIORITY}
  done

  for i in ${JRE}; do
    update-alternatives --install /usr/bin/"${i}" "${i}" ${DEST}/${TYPE}${MAJOR}/bin/"${i}" ${PRIORITY}
  done

  if [ ${TYPE} == 'jdk' ]; then
    for i in ${JDK}; do
      update-alternatives --install /usr/bin/"${i}" "${i}" ${DEST}/${TYPE}${MAJOR}/bin/"${i}" ${PRIORITY}
    done

    for i in ${BROWSER}; do
      update-alternatives --install /usr/lib/"${i}"/plugins/libjavaplugin.so "${i}"-javaplugin.so ${DEST}/${TYPE}${MAJOR}/jre/lib/${BITS}/${PLUGIN} ${PRIORITY}
    done
  fi

  if [ ${TYPE} == 'jre' ]; then
    for i in ${BROWSER}; do
      update-alternatives --install /usr/lib/"${i}"/plugins/libjavaplugin.so "${i}"-javaplugin.so ${DEST}/${TYPE}${MAJOR}/lib/${BITS}/${PLUGIN} ${PRIORITY}
    done
  fi
}

function _isSuperUser {
  # Test if you're in super-user mode
  if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
  fi
}

function usage {
  echo
  echo "Usage: sudo install-java.sh <tar.gz> "
  echo
  echo "Install and configure Oracle JDK or JRE on Ubuntu."
  echo "Remove preview installed version and add it to update-alternatives to"
  echo "simplify the switch between different versions."
  echo
  echo " Parameters:"
  echo "  -h,   --help       Display this help then quit"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Launch part
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ $# -ne 1 ]; then
  usage
  exit 1
fi

# retrieve command parameters
while [ "$1" != "" ]; do
  case $1 in
    -h | --help ) usage
                  exit
                  ;;
    * )           _isSuperUser "$1"
                  split_tarfile "$1"
                  install "$1"
                  create_jinfo "$1"
                  update_alternatives "$1"
                  ;;
  esac
  shift
done
