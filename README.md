Oracle Java installer for Ubuntu
================================

`install-java.sh` is a small script designed to simplify the installation of the Oracle JRE or JDK on Ubuntu.  
(FYI, Oracle no longer provide Debian packages since they acquired Sun Microsystems)

This script will remove previous installed version with the same major revision number then install the new one and add it to your system.

##Usage

1. Download the tar.gz file of JDK or JRE :

   http://www.oracle.com/technetwork/java/javase/downloads/index.html

2. Run the installation script :

   `$ bash install-java.sh <tar.gz>`

3. Enable the new JDK / JRE on your system :
  1. Enable only a speficic command like `java`

     `$ sudo update-alternative --config java`

  2. Completely switch the Java version

     ```
     # list all installed version of java
     $ sudo update-java-alternatives -l

     # select the new version to use
     $ update-java-alternatives -s <java_version>
     ```

4. Check the Java version :

   ```$ java --version```

## References
- http://askubuntu.com/questions/141791/is-there-a-way-to-update-all-java-related-alternatives
- http://askubuntu.com/questions/21131/how-to-correctly-remove-openjdk-openjre-and-set-sunjdk-sunjre-as-default
- https://gist.github.com/cwchien/1114515/
- https://gist.github.com/cwchien/1114571/
